package com.naphaso.oktest.server;

import com.naphaso.oktest.property.ApplicationProperties;
import com.naphaso.oktest.property.ServerProperties;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.SoftReferenceObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;

/**
 * Created by wolong on 11/12/13.
 */
public class NIOServer {
    private static final Logger logger = LoggerFactory.getLogger(NIOServer.class);

    private final ApplicationProperties properties;

    private final IdentityHashMap<ServerSocketChannel, ServerProperties> serverChannelProperties = new IdentityHashMap<ServerSocketChannel, ServerProperties>();
    private final IdentityHashMap<SocketChannel, SocketChannel> channelAssociation = new IdentityHashMap<SocketChannel, SocketChannel>();
    private final IdentityHashMap<SocketChannel, Queue<ByteBuffer>> channelWriteQueue = new IdentityHashMap<SocketChannel, Queue<ByteBuffer>>();

    private ObjectPool<ByteBuffer> bufferObjectPool = new SoftReferenceObjectPool<ByteBuffer>(new PoolableObjectFactory<ByteBuffer>() {
        @Override
        public ByteBuffer makeObject() throws Exception {
            return ByteBuffer.allocate(16384);
        }

        @Override
        public void destroyObject(ByteBuffer byteBuffer) throws Exception {

        }

        @Override
        public boolean validateObject(ByteBuffer byteBuffer) {
            return true;
        }

        @Override
        public void activateObject(ByteBuffer byteBuffer) throws Exception {

        }

        @Override
        public void passivateObject(ByteBuffer byteBuffer) throws Exception {
            byteBuffer.clear();
        }
    });

    public NIOServer(ApplicationProperties properties) {
        this.properties = properties;
    }

    private Selector socketSelector;

    public void run() throws Exception {
        // initialize selector
        socketSelector = SelectorProvider.provider().openSelector();

        // setup listen socket on each server
        for(ServerProperties serverProperties : properties.getServers().values()) {
            logger.info("setup server {}", serverProperties.getName());
            final InetSocketAddress inetSocketAddress = new InetSocketAddress(serverProperties.getLocalHost(), serverProperties.getLocalPort());
            final ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(inetSocketAddress);
            serverSocketChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
            serverChannelProperties.put(serverSocketChannel, serverProperties);
        }

        // data transfer loop
        while(true) {
            socketSelector.select();
            final Iterator<SelectionKey> selectionKeyIterator = socketSelector.selectedKeys().iterator();
            while(selectionKeyIterator.hasNext()) {
                final SelectionKey selectionKey = selectionKeyIterator.next();
                selectionKeyIterator.remove();
                if(!selectionKey.isValid()) {
                    continue;
                }

                if(selectionKey.isAcceptable()) {
                    acceptChannel(selectionKey);
                } else if(selectionKey.isReadable()) {
                    readChannel(selectionKey);
                } else if(selectionKey.isWritable()) {
                    writeChannel(selectionKey);
                } else if(selectionKey.isConnectable()) {
                    connectChannel(selectionKey);
                }
            }
        }
    }

    private void acceptChannel(SelectionKey selectionKey) {
        final ServerSocketChannel serverChannel = (ServerSocketChannel) selectionKey.channel();
        final ServerProperties serverProperties = serverChannelProperties.get(serverChannel);


        try {
            // setup client channel
            final SocketChannel clientChannel = serverChannel.accept();

            logger.info("new client {} on server {} is connected", clientChannel.getRemoteAddress(), serverProperties.getName());
            logger.info("pool status: active {}, idle {}", bufferObjectPool.getNumActive(), bufferObjectPool.getNumIdle());

            clientChannel.configureBlocking(false);
            clientChannel.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
            clientChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);

            // setup connect channel
            final SocketChannel connectChannel = SocketChannel.open();
            final InetSocketAddress remoteAddress = new InetSocketAddress(serverProperties.getRemoteHost(), serverProperties.getRemotePort());
            connectChannel.configureBlocking(false);
            connectChannel.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
            connectChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
            connectChannel.connect(remoteAddress);

            // register channels
            clientChannel.register(socketSelector, SelectionKey.OP_READ);
            connectChannel.register(socketSelector, SelectionKey.OP_CONNECT);

            // setup write queue and channel association
            addChannelAssociation(clientChannel, connectChannel);
        } catch (ClosedChannelException e) {
            logger.debug("some exception", e);
        } catch (IOException e) {
            logger.debug("some exception", e);
        }
    }

    private void readChannel(SelectionKey selectionKey) {
        final SocketChannel clientChannel = (SocketChannel) selectionKey.channel();
        final SocketChannel connectChannel = channelAssociation.get(clientChannel);
        final Queue<ByteBuffer> writeQueue = channelWriteQueue.get(connectChannel);

        // ugly try chain because of Apache Object Pool exceptions
        try {
            final ByteBuffer buffer = bufferObjectPool.borrowObject();
            try {
                int len = clientChannel.read(buffer);
                if(len == -1) {
                    logger.debug("graceful close connection");
                    selectionKey.cancel();
                    bufferObjectPool.returnObject(buffer);
                    writeQueue.add(null);
                } else if(len > 0) {
                    buffer.flip();
                    writeQueue.add(buffer);
                }
            } catch (IOException e) {
                logger.debug("read error", e);
                bufferObjectPool.returnObject(buffer);
                selectionKey.cancel();
                writeQueue.add(null);
            }

            try {
                if(connectChannel.isConnected()) {
                    connectChannel.register(socketSelector, SelectionKey.OP_WRITE | SelectionKey.OP_READ);
                }
            } catch (ClosedChannelException e) {
                // nothing
            }
        } catch (Exception e) {
            logger.error("pool exception");
        }
    }

    private void writeChannel(SelectionKey selectionKey) {
        final SocketChannel connectChannel = (SocketChannel) selectionKey.channel();
        final Queue<ByteBuffer> writeQueue = channelWriteQueue.get(connectChannel);

        try {
            while(!writeQueue.isEmpty()) {
                final ByteBuffer buffer = writeQueue.poll();

                if(buffer == null) {
                    logger.debug("channel termination");
                    destroyChannelAssociation(connectChannel);
                    return;
                }

                try {
                    connectChannel.write(buffer);
                } finally {
                    bufferObjectPool.returnObject(buffer);
                }
            }

            selectionKey.interestOps(SelectionKey.OP_READ);
        } catch (Exception e) {
            logger.debug("write error", e);
            selectionKey.cancel();
            destroyChannelAssociation(connectChannel);
        }
    }

    private void connectChannel(SelectionKey selectionKey) {
        final SocketChannel connectChannel = (SocketChannel) selectionKey.channel();
        final Queue<ByteBuffer> writeQueue = channelWriteQueue.get(connectChannel);
        try {
            connectChannel.finishConnect();
            if(writeQueue.isEmpty()) {
                selectionKey.interestOps(SelectionKey.OP_READ);
            } else {
                selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
            }
        } catch (ClosedChannelException e) {
            logger.debug("connection error", e);
            selectionKey.cancel();
            destroyChannelAssociation(connectChannel);
        } catch (IOException e) {
            logger.debug("connection error", e);
            selectionKey.cancel();
            destroyChannelAssociation(connectChannel);
        }
    }


    private void addChannelAssociation(SocketChannel channel1, SocketChannel channel2) {
        channelWriteQueue.put(channel1, new LinkedList<ByteBuffer>());
        channelWriteQueue.put(channel2, new LinkedList<ByteBuffer>());
        channelAssociation.put(channel1, channel2);
        channelAssociation.put(channel2, channel1);
    }

    private void destroyChannelAssociation(SocketChannel channel1) {
        final SocketChannel channel2 = channelAssociation.remove(channel1);
        returnQueueToPool(channelWriteQueue.remove(channel1));
        try { channel1.close(); } catch (IOException e) {}

        if(channel2 != null) {
            channelAssociation.remove(channel2);
            returnQueueToPool(channelWriteQueue.remove(channel2));
            try { channel2.close(); } catch (IOException e) {}
        }
    }

    private void returnQueueToPool(Queue<ByteBuffer> queue) {
        ByteBuffer buffer = queue.poll();
        while(buffer != null) {
            try {
                bufferObjectPool.returnObject(buffer);
            } catch (Exception e) {
                logger.error("returning buffer to pool exception", e);
            }
            buffer = queue.poll();
        }
    }
}
