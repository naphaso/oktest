package com.naphaso.oktest.server.netty;

import com.naphaso.oktest.property.ServerProperties;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by wolong on 11/12/13.
 */
public class ExchangeChannelInitializer extends ChannelInitializer<SocketChannel> {
    private static final Logger logger = LoggerFactory.getLogger(ExchangeChannelHandler.class);

    private final ServerProperties serverProperties;
    private final EventLoopGroup clientGroup = new NioEventLoopGroup();

    public ExchangeChannelInitializer(ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        logger.debug("initialize channel");
        final ExchangeChannelHandler handler = new ExchangeChannelHandler(serverProperties);
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(clientGroup)
                .channel(NioSocketChannel.class)
                .handler(handler.getConnectHandler());

        logger.debug("connect to {}:{}", serverProperties.getRemoteHost(), serverProperties.getRemotePort());

        bootstrap.connect(serverProperties.getRemoteHost(), serverProperties.getRemotePort());

        ch.pipeline().addLast(handler);
    }
}
