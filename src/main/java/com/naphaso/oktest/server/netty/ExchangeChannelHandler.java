package com.naphaso.oktest.server.netty;

import com.naphaso.oktest.property.ServerProperties;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by wolong on 11/12/13.
 */
public class ExchangeChannelHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(ExchangeChannelHandler.class);

    private final ServerProperties serverProperties;
    private final ByteBuf clientToConnectBuffer = Unpooled.buffer();
    private final ByteBuf connectToClientBuffer = Unpooled.buffer();
    private volatile ChannelHandlerContext clientChannel = null;
    private volatile ChannelHandlerContext connectChannel = null;

    public ExchangeChannelHandler(ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        clientChannel = ctx;
        synchronized (connectToClientBuffer) {
            if(connectToClientBuffer.readableBytes() != 0) {
                clientChannel.writeAndFlush(connectToClientBuffer);
            }
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        clientChannel = null;
        ctx.close();
        if(connectChannel != null) {
            connectChannel.close();
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        writeToConnect((ByteBuf) msg);
    }

    public void writeToClient(ByteBuf buffer) {
        if(clientChannel != null) {
            clientChannel.writeAndFlush(buffer);
        } else {
            synchronized (connectToClientBuffer) {
                connectToClientBuffer.writeBytes(buffer);
            }
        }
    }

    public void writeToConnect(ByteBuf buffer) {
        if(connectChannel != null) {
            connectChannel.writeAndFlush(buffer);
        } else {
            synchronized (clientToConnectBuffer) {
                clientToConnectBuffer.writeBytes(buffer);
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.debug("client exception", cause);
        ctx.close();
        if(connectChannel != null) {
            connectChannel.close();
        }
    }

    ChannelHandler getConnectHandler() {
        return new ChannelInboundHandlerAdapter() {
            @Override
            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                connectChannel = ctx;
                synchronized (clientToConnectBuffer) {
                    if(clientToConnectBuffer.readableBytes() != 0) {
                        connectChannel.writeAndFlush(clientToConnectBuffer);
                    }
                }
            }

            @Override
            public void channelInactive(ChannelHandlerContext ctx) throws Exception {
                connectChannel = null;
                ctx.close();
                if(clientChannel != null) {
                    clientChannel.close();
                }
            }

            @Override
            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                writeToClient((ByteBuf) msg);
            }

            @Override
            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
                logger.debug("connect exception", cause);
                ctx.close();
                if(clientChannel != null) {
                    clientChannel.close();
                }
            }
        };
    }
}
