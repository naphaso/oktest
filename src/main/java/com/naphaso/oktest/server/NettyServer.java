package com.naphaso.oktest.server;

import com.naphaso.oktest.server.netty.ExchangeChannelInitializer;
import com.naphaso.oktest.property.ApplicationProperties;
import com.naphaso.oktest.property.ServerProperties;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wolong on 11/12/13.
 */
public class NettyServer {
    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    private final ApplicationProperties properties;

    public NettyServer(ApplicationProperties properties) {
        this.properties = properties;
    }

    private final EventLoopGroup bossGroup = new NioEventLoopGroup();
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();

    public void run() throws Exception {
        for(ServerProperties serverProperties : properties.getServers().values()) {
            logger.info("setup server {}", serverProperties.getName());

            final ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ExchangeChannelInitializer(serverProperties))
                    .option(ChannelOption.SO_KEEPALIVE, true);

            bootstrap.bind(serverProperties.getLocalHost(), serverProperties.getLocalPort());
        }
    }
}
