package com.naphaso.oktest.exception;

/**
 * Created by wolong on 11/11/13.
 */
public class ApplicatonException extends Exception {
    public ApplicatonException(String message) {
        super(message);
    }

    public ApplicatonException(String message, Throwable cause) {
        super(message, cause);
    }
}
