package com.naphaso.oktest.exception;

/**
 * Created by wolong on 11/11/13.
 */
public class ConfigurationException extends ApplicatonException {
    public ConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
