package com.naphaso.oktest;

import com.naphaso.oktest.property.ApplicationProperties;
import com.naphaso.oktest.server.NIOServer;
import com.naphaso.oktest.server.NettyServer;
import org.apache.commons.cli.*;

public class App {
    public static void main(String[] args) throws Exception {
        final Options options = new Options();
        options.addOption("f", "config-file", true, "Path to configuration file");
        options.addOption("e", "engine", true, "Server engine, nio or netty");

        final CommandLineParser commandLineParser = new GnuParser();
        final CommandLine commandLine = commandLineParser.parse(options, args);

        final String configFile = commandLine.getOptionValue("f", "config.properties");
        final String engine = commandLine.getOptionValue("e");

        if(engine == null) {
            final HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("portmapper", "Java port-mapping utility", options, "Author: Stanislav Ovsyannikov", true);
            return;
        }

        final ApplicationProperties properties = new ApplicationProperties(configFile);
        if(engine.equals("nio")) {
            final NIOServer server = new NIOServer(properties);
            server.run();
        } else if(engine.equals("netty")) {
            final NettyServer server = new NettyServer(properties);
            server.run();
        } else {
            final HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("portmapper", "Java port-mapping utility", options, "Author: Stanislav Ovsyannikov", true);
        }
    }
}
