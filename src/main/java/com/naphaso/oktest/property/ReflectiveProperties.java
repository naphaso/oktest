package com.naphaso.oktest.property;

import com.naphaso.oktest.exception.ConfigurationException;

import java.lang.reflect.Field;

/**
 * Created by wolong on 11/11/13.
 */
public abstract class ReflectiveProperties {
    public void setProperty(String key, String value) throws ConfigurationException {
        Field field;
        try {
            field = this.getClass().getDeclaredField(key);
        } catch (NoSuchFieldException e) {
            throw new ConfigurationException("unknown configuration field: " + key);
        }

        field.setAccessible(true);

        try {
            field.set(this, value);
        } catch (IllegalAccessException e) {
            throw new ConfigurationException("invalid field type: " + field.getClass().getCanonicalName());
        }
    }
}
