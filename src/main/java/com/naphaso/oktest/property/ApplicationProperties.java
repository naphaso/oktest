package com.naphaso.oktest.property;

import com.naphaso.oktest.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * Created by wolong on 11/11/13.
 */
public class ApplicationProperties {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);

    private HashMap<String, ServerProperties> servers = new HashMap<String, ServerProperties>();

    public ApplicationProperties(String fileName) throws ConfigurationException {
        load(fileName);
    }

    public void load(String fileName) throws ConfigurationException {
        try {
            Properties properties = new Properties();
            InputStream inputStream = new FileInputStream(fileName);
            properties.load(inputStream);

            for(String propertyName : properties.stringPropertyNames()) {
                StringTokenizer tokenizer = new StringTokenizer(propertyName, ".");
                if(tokenizer.countTokens() != 2) {
                    throw new ConfigurationException("invalid property: " + propertyName);
                }

                String serverName = tokenizer.nextToken();
                String param = tokenizer.nextToken();

                ServerProperties serverProperties = servers.get(serverName);
                if(serverProperties == null) {
                    serverProperties = new ServerProperties(serverName);
                    servers.put(serverName, serverProperties);
                }

                serverProperties.setProperty(param, properties.getProperty(propertyName));
            }

            for(Map.Entry<String, ServerProperties> entry : servers.entrySet()) {
                final String name = entry.getKey();
                final ServerProperties server = entry.getValue();
                server.validate();
                logger.info("loaded server {}: localHost: {}, localPort: {}, remoteHost: {}, remotePort: {}", name, server.getLocalHost(), server.getLocalPort(), server.getRemoteHost(), server.getRemotePort());
            }
        } catch (FileNotFoundException e) {
            throw new ConfigurationException("configuration file not found");
        } catch (IOException e) {
            throw new ConfigurationException("error read configuration file", e);
        }
    }

    public HashMap<String, ServerProperties> getServers() {
        return servers;
    }
}
