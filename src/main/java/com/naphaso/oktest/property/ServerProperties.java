package com.naphaso.oktest.property;

import com.naphaso.oktest.exception.ConfigurationException;

/**
 * Created by wolong on 11/11/13.
 */
public class ServerProperties extends ReflectiveProperties {
    private String name;
    private String localHost = "0.0.0.0";
    private String localPort;
    private String remoteHost;
    private String remotePort;

    public ServerProperties(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getLocalHost() {
        return localHost;
    }

    public int getLocalPort() {
        return Integer.parseInt(localPort);
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public int getRemotePort() {
        return Integer.parseInt(remotePort);
    }

    public void validate() throws ConfigurationException {
        try {
            Integer.parseInt(localPort);
            Integer.parseInt(remotePort);
        } catch (NumberFormatException e) {
            throw new ConfigurationException("invalid port value");
        }
    }
}
